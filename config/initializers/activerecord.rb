require 'active_record/connection_adapters/mysql_adapter'
require 'active_record/connection_adapters/sqlite3_adapter'

class ActiveRecord::ConnectionAdapters::MysqlAdapter
  NATIVE_DATABASE_TYPES[:primary_key] = "int(11) auto_increment PRIMARY KEY"
end

class ActiveRecord::ConnectionAdapters::SQLiteAdapter::SQLite3Adapter # :nodoc:
    def table_structure(table_name)
      returning structure = @connection.table_info(table_name) do
        raise(ActiveRecord::StatementInvalid, "Could not find table '#{table_name}'") if structure.empty?
      end
    end
end

